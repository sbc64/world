import React from 'react';
import Helmet from 'react-helmet';
import Layout from '../../../../components/layout';
import Gallery from '../../../../components/Gallery';

import Images from './Images'

class HomeIndex extends React.Component {

  constructor() {
    super();

    this.state = {
      lightboxIsOpen: false,
      currentImage: 0,
    };
  }

  render() {
    const siteTitle = 'Budapest';
    const siteDescription = 'Prague to Budapest';

    return (
      <Layout>
        <Helmet>
          <title>{siteTitle}</title>
          <meta
            name='description' content={siteDescription}
          />
        </Helmet>

        <div id='main'>

          <section id='one'>
            <header className='major'>
              <h2>Onwards to Budapest!</h2>
            </header>
          </section>

          <section id='text'>
            <p>
      So I have been in Budapest for five days. <a href="https://nomadlist.com/budapest">Nomadlist.com</a> ranks it as one of the cheapest cities in eastern Europe so I might stay here a little bit longer. The hostel I'm staying at is nice and clean. Not that many people. I feel that my possesions are safe in there so that is good. It is also an extremely walkable city. You basically only need to stay close to the city center of the Pest side and can be anywhere of interest within 30 minutes. I haven't tried the public transportation here yet... but roughly speaking Budapest seems like a newer Prague.
            </p>
            <p>
      There are donner kebab stands everywhere. Barber shops are also everywhere (the universe is telling me something here). The food is good and cheap. Budapest is a really appealing city to live in while nomading. Some of the other digital nomads I have met also agree with me. I met one of the organizers of <a href="https://7in7.co/">https://7in7.co</a> which is neat. I have received a lot of good tips from her about the nomadic lifestyle. I'm also currently working with a winner of the #crytolife hackathon. Which exactly the kind of people I want to be around with. There are also like 8 different bouldering gyms. I still need to check one out but I'm pretty exited that there are EIGHT bouldering gyms in Budapest.
            </p>
            <p>
      On Sunday, November 11th, We did tourist walk with my friend Nate and first went to the highest viewpoint of the city. Our objective was to go to the steam baths but we sortha started walking around everywhere. After exploring the castle and the other viewpoints we went to the thermal baths.. zzzzz. Our last stop was the <a href="http://budacastlebudapest.com/budapest-history-museum/">Budapest History Museum</a>. It was a good 10 mile walk we did for the day.
            </p>
            <p>
      Budapest overall has been more mellow. I have been going to <a href="http://cafe.magveto.hu/">coffee shop</a> and working, still happy that I can work remotely. <a href="http://fricipapa.hu/">Frica Papa</a> restaurant is really tasty and cheap. If you want to read an interesting book, <a hred="https://www.amazon.com/Skin-Game-Hidden-Asymmetries-Daily/dp/042528462X">Skin in the Game</a> is thoroughly entertaining.
            </p>
            <p>
           Till next time! 
            </p>
          </section>

          <section id='gallery'>
            <Images />
          </section>
        </div>
      </Layout>
    );
  }
}

export default HomeIndex;
