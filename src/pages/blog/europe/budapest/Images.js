import React from 'react';
import Gallery from '../../../../components/Gallery';


import train from '../../../../assets/images/fulls/budapest/train.jpg';
import astoria from '../../../../assets/images/fulls/budapest/astoria.jpg';
import compass from '../../../../assets/images/fulls/budapest/compass.jpg';
import glass  from '../../../../assets/images/fulls/budapest/glass.jpg';
import ruin from '../../../../assets/images/fulls/budapest/ruin.jpg';
import goingBack from '../../../../assets/images/fulls/budapest/goingBack.jpg';
import bazaar from '../../../../assets/images/fulls/budapest/bazaar.jpg';
import baths from '../../../../assets/images/fulls/budapest/baths.jpg';
import hostel from '../../../../assets/images/fulls/budapest/hostel.jpg';
import pest from '../../../../assets/images/fulls/budapest/pest.jpg';
import museum from '../../../../assets/images/fulls/budapest/museum.jpg';

const images = [
  { 
    src: train,
    thumbnail: train,
    description: 'Train from Prague to Budapest'
  },
  { 
    src: compass,
    thumbnail: compass,
    description: 'This compass has been a life saver'
  },
  { 
    src: bazaar,
    thumbnail: bazaar,
    description: 'One of the many bazaars in this city'
  },
  { 
    src: ruin,
    thumbnail: ruin,
    description: 'Ruin bar with neat design'
  },
  { 
    src:  pest,
    thumbnail: pest,
    description: 'View point of Pest, from Buda'
  },
  { 
    src:  hostel,
    thumbnail: hostel,
    //eslint-disable-next-line
    description: `Hostel where I'm staying at`
  },
  { 
    src:  baths,
    thumbnail: baths,
    description: 'The infamous Budapest thermal baths'
  },
  { 
    src:  glass,
    thumbnail: glass,
    description: 'My digital nomad friend that I have been hanging out with'
  },
  { 
    src:  museum,
    thumbnail: museum,
    description: 'The Hungarian Historical Museum is a cool place. It is essentially maze of old ruins built on top of each other since Roman times'
  },
  { 
    src:  goingBack,
    thumbnail: goingBack,
    description: 'Going back to Pest after a long day walking'
  },
  { 
    src:  astoria,
    thumbnail: astoria,
    //eslint-disable-next-line
    description: `Don't worry, I'm just a metro ride away from Oregon ;-)`
  },
];

export default class Images extends React.Component {
  constructor() {
    super();
    this.images = images;
  }

  render() {
    return(
      <Gallery
        images={
          images.map(({
            src,
            thumbnail,
            caption,
            description
          }) => ({
            src,
            thumbnail,
            caption,
            description
          }))}
      />
    );
  }
}
