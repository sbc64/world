import React from 'react';
import Gallery from '../../../../components/Gallery';


import tree from '../../../../assets/images/fulls/tree.jpg';
import equipment from '../../../../assets/images/fulls/prague/my_equipment.jpg';
import cartel from '../../../../assets/images/fulls/prague/first_meta_cartel.jpg';
import hacking from '../../../../assets/images/fulls/prague/hacking.jpg';
import office from '../../../../assets/images/fulls/prague/office.jpg';
import roommies from '../../../../assets/images/fulls/prague/roommates.jpg';
import boshiv from '../../../../assets/images/fulls/prague/river_bo_shiv.jpg';
import coffeeHacking from '../../../../assets/images/fulls/prague/coffe_hacking.jpg';
import cryptolife from '../../../../assets/images/fulls/prague/cryptolife.jpg';
import rabit from '../../../../assets/images/fulls/prague/rabit.jpg';
import devcon from '../../../../assets/images/fulls/prague/devcon.jpg';
import midday from '../../../../assets/images/fulls/prague/midday.jpg';
import sunset from '../../../../assets/images/fulls/prague/sunset.jpg';
import astronomical from '../../../../assets/images/fulls/prague/astrological.jpg';
import gargoyles from '../../../../assets/images/fulls/prague/gargoyles.jpg';
import garden from '../../../../assets/images/fulls/prague/garden.jpg';
import selfie from '../../../../assets/images/fulls/prague/river_selfie.jpg';
import kafta from '../../../../assets/images/fulls/prague/kafta.jpg';
import club from '../../../../assets/images/fulls/prague/club.jpg';
import metal from '../../../../assets/images/fulls/prague/metal.jpg';
import institute from '../../../../assets/images/fulls/prague/crypto_anarchy.jpg';
import toby from '../../../../assets/images/fulls/prague/toby.jpg';

const images = [
  { 
    src: hacking,
    thumbnail: hacking,
    description: 'First part of my adventure was in San Francisco'
  },
  { 
    src: cartel,
    thumbnail: cartel,
    description: 'Making new friends with the web3 community'
  },
  { 
    src: office,
    thumbnail: office,
    description: 'A coworking space I crashed at in SF'
  },
  { 
    src: roommies,
    thumbnail: roommies,
    description: 'I had to leave some fantastic roommates'
  },
  { 
    src: tree,
    thumbnail: tree,
    description: 'Picture of a park in Portland'
  },
  {
    src: equipment,
    thumbnail: equipment,
    description: 'All my belongings. I counted 113 items'
  },
  { 
    src: boshiv,
    thumbnail: boshiv,
    description: 'Some new roommates/best friends which are also some awesome hackers'
  },
  { 
    src: cryptolife,
    thumbnail: cryptolife,
    description: 'Venue for the #cryptolife hackathon'
  },
  { 
    src: coffeeHacking,
    thumbnail: coffeeHacking,
    description: 'Working from a coffee shop'
  },
  { 
    src: rabit,
    thumbnail: rabit,
    description: 'This coffee shop had an awesome art gallery'
  },
  { 
    src: devcon,
    thumbnail: devcon,
    description: 'Devcon, an Ethereum conference'
  },
  { 
    src: midday,
    thumbnail: midday,
    description: 'View from the conference center'
  },
  { 
    src: sunset,
    thumbnail: sunset,
    description: 'Beautiful sunset over Prague'
  },
  { 
    src: astronomical,
    thumbnail: astronomical,
    description: 'The infamous Astrological clock. Go there early morning if you want to avoid a hoard of tourists'
  },
  { 
    src: gargoyles,
    thumbnail: gargoyles,
    description: 'Each gargoyle at the Prague castle was unique'
  },
  { 
    src: garden,
    thumbnail: garden,
    description: 'Hole in the wall garden with mulled wine... hot drinks are going to be essential plus they make me a happy drunk'
  },
  { 
    src: selfie,
    thumbnail: selfie,
    description: 'Just me over the Charles bridge with the castle in the backgorund'
  },
  { 
    src: kafta,
    thumbnail: kafta,
    description: 'Picture with my good friends Shayan an Anna before entering the Franz Kafta museum.'
  },
  { 
    src: club,
    thumbnail: club,
    description: 'Really cool club, Cross Club'
  },
  { 
    src: metal,
    thumbnail: metal,
    description: 'The club had a steam punk vibe with a bunch of cut up motherboards inside'
  },
  { 
    src: institute,
    thumbnail: institute,
    description: 'Institue of Cryptoanarchy. This place is a coworking space and a coffee shop that only takes payments in Bitcoin, and other crypto currencies'
  },
  { 
    src: toby,
    thumbnail: toby,
    description: 'Picture of my laptop while writing this blog post. I like the aesthetic of my laptop so I think it will be the main protagonist of my euro trip'
  },
];

export default class Images extends React.Component {
  constructor() {
    super();
    this.images = images;
  }

  render() {
    return(
      <Gallery
        images={
          images.map(({
            src,
            thumbnail,
            caption,
            description
          }) => ({
            src,
            thumbnail,
            caption,
            description
          }))}
      />
    );
  }
}
