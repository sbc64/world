import React from 'react';
import Gallery from '../../../../components/Gallery';

import bla

/*
 *
 * So the issue is that gatsby uses static references for the images and it only generates these images from an import.
 * it canno't read from a src path. it needs the compiled image
 */

let pre = '../../../../assets/images/fulls/cracovia/';
let images = [
  {src: 'baklava.jpg',description: ''},
  {src: 'balancingBridge.jpg', description: ''},
  {src: 'balancing.jpg', description: ''},
  {src: 'balancingSung.jpg', description: ''},
  {src: 'btc.jpg', description: ''},
  {src: 'chruchBug.jpg', description: ''},
  {src: 'copernicus.jpg', description: ''},
  {src: 'deadPeople.jpg', description: ''},
  {src: 'dogStatue.jpg', description: ''},
  {src: 'dogText.jpg', description: ''},
  {src: 'dragonsStatues.jpg', description: ''},
  {src: 'fortMe.jpg', description: ''},
  {src: 'fortPark.jpg', description: ''},
  {src: 'fortSigns.jpg', description: ''},
  {src: 'fortSnow.jpg', description: ''},
  {src: 'gloomy.jpg', description: ''},
  {src: 'omellete.jpg', description: ''},
  {src: 'panoramic.jpg', description: ''},
  {src: 'snowPark.jpg', description: ''},
  {src: 'tramRails.jpg', description: ''},
].map(i => {
  return {
    src:pre + i.src,
    descriptions: i.description,
    caption: '',
    thumbnail: pre + i.src
  };
});

export default class Images extends React.Component {
  render() {
    console.log('SUP', images[0].src)
    return(
      <Gallery
        images={
          images.map(({
            src,
            thumbnail,
            caption,
            description
          }) => ({
            src,
            thumbnail,
            caption,
            description
          }))
        }
      />
    );
  }
}
